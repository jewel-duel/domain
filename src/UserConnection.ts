import type { User } from "./Users/User";
import type { Connection } from "./Connection";

export class UserConnection {
  #user: User;
  #connection: Connection;

  constructor(user: User, connection: Connection) {
    this.#connection = connection;
    this.#user = user;
  }

  get connection(): Connection {
    return this.#connection;
  }

  get user(): User {
    return this.#user;
  }
}
