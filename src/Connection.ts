import { generate } from "./Utils/v4";

export class Connection {
  #ws: WebSocket;
  #id: string;

  protected _id: string;

  public static Create(websocket: WebSocket): Connection {
    return new Connection(websocket);
  }

  private constructor(ws: WebSocket) {
    this.#ws = ws;
    this.#id = generate();
    this._id = this.#id;
  }

  get websocket(): WebSocket {
    return this.#ws;
  }

  get id(): string {
    return this.#id;
  }
}
