export class HexCoord {
  private _q: number;
  private _r: number;
  private _s: number;

  public static Create(q: number, r: number, s: number): HexCoord {
    return new HexCoord(q, r, s);
  }

  public static Origin(): HexCoord {
    return new HexCoord(0, 0, 0);
  }

  public static FromString(keyString: string): HexCoord {
    let coords = keyString.split(".");
    return new HexCoord(
      parseInt(coords[0]),
      parseInt(coords[1]),
      parseInt(coords[2]),
    );
  }

  private constructor(q: number, r: number, s: number) {
    if (q + r + s !== 0) {
      throw new Error(`Impossible hex coordinates (${q}, ${r}, ${s})`);
    }

    this._q = q;
    this._r = r;
    this._s = s;
  }

  get q(): number {
    return this._q;
  }

  get r(): number {
    return this._r;
  }

  get s(): number {
    return this._s;
  }

  public shiftRight(): HexCoord {
    return HexCoord.Create(this._s, this._q, this._r);
  }

  public shiftLeft(): HexCoord {
    return HexCoord.Create(this._r, this._s, this._q);
  }

  public multiply(multiple: number): HexCoord {
    return HexCoord.Create(
      this._q * multiple,
      this._r * multiple,
      this._s * multiple,
    );
  }

  public add(coord: HexCoord): HexCoord {
    return HexCoord.Create(
      this._q + coord.q,
      this._r + coord.r,
      this._s + coord.s,
    );
  }

  public subtract(coord: HexCoord): HexCoord {
    return HexCoord.Create(
      this._q - coord.q,
      this._r - coord.r,
      this._s - coord.s,
    );
  }

  public toString(): string {
    return `${this._q}.${this._r}.${this._s}`;
  }

  public toArray(): number[] {
    return [this._q, this._r, this._s];
  }
}

export class HexVector {
  private _axis: Axis;
  private _direction: number;

  public static Up(axis: Axis): HexVector {
    return new HexVector(axis, 1);
  }

  public static Down(axis: Axis): HexVector {
    return new HexVector(axis, -1);
  }

  private constructor(axis: Axis, direction: number) {
    this._axis = axis;
    this._direction = direction;
  }

  get direction(): number {
    return this._direction;
  }

  get axis(): Axis {
    return this._axis;
  }
}

export enum Axis {
  Q,
  R,
  S,
}
