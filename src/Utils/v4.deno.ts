//@ts-nocheck
import { v4 } from "../../deps.ts";

export function generate(): string {
  return v4.generate();
}
