// Copyright 2018-2021 the Deno authors. All rights reserved. MIT license.
import { v4 as uuidv4 } from "uuid";

/** Generates a RFC4122 v4 UUID (pseudo-randomly-based) */
export function generate(): string {
  return uuidv4();
}
