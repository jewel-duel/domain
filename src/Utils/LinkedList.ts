export namespace DS {
  export class Node<T> {
    public item: T | null;
    public next: Node<T> | null;

    public constructor(item: T | null = null) {
      this.item = item;
      this.next = null;
    }
  }

  export class DoubleNode<T> {
    public value: T | null;

    public prev: DoubleNode<T> | null;
    public next: DoubleNode<T> | null;

    public constructor(
      item: T | null = null,
      prev: DoubleNode<T> | null = null,
      next: DoubleNode<T> | null = null,
    ) {
      this.value = item;
      this.prev = prev;
      this.next = next;
    }

    public hasPrev(): boolean {
      return this.prev !== null;
    }

    public hasNext(): boolean {
      return this.next !== null;
    }
  }
}

export class DoubleLinkedList<T> {
  private _head: DS.DoubleNode<T>;
  private _tail: DS.DoubleNode<T>;
  private _length: number;

  public constructor() {
    this._length = 0;
    this._head = new DS.DoubleNode<T>(null);
    this._tail = new DS.DoubleNode<T>(null);
  }

  public isEmpty(): boolean {
    return this._length === 0;
  }

  /**
   * Insert a new value at the specified index
   *
   * @method add
   * @param {number} index The index where the new value is to be inserted.
   * @param {T} value The new value for the index.
   * @returns void
   * @memberof DoubleLinkedList
   */
  public add(index: number, value: T): void {
    if (index < 0 || index >= this._length) {
      throw new Error(`Index of ${index} is out of bounds.`);
    }

    let i = 0;
    let current = this._head;
    while (i < index) {
      current = current.next!;
      i++;
    }

    current.value = value;
  }

  /**
   * Pops a node from the end of the double linked list.
   *
   * @method pop
   * @returns {T} The value of the popped node.
   * @memberof DoubleLinkedList
   */
  public pop(): T {
    if (this.isEmpty()) {
      throw new Error("List is empty.");
    }

    let value = this._tail.value!;

    this._tail = this._tail.prev!;
    if (this._tail) {
      this._tail.next = null;
    }

    this._length--;

    if (this.isEmpty()) {
      this._head = new DS.DoubleNode<T>(null);
    }

    return value;
  }

  /**
   * Pushes an element at the end of the doubly linked list.
   *
   * @method push
   * @param {T} value The value to push
   * @memberof DoubleLinkedList
   */
  public push(value: T): void {
    const node = new DS.DoubleNode<T>(value, this._tail, null);
    if (this.isEmpty()) {
      this._head = this._tail = node;
    } else {
      this._tail.next = node;
      this._tail = this._tail.next;
    }

    this._length++;
  }

  /**
   * Shifts a node from the beginning of the doubly linked list.
   *
   * @method shift
   * @returns {T} The value of the shifted node.
   * @memberof DoubleLinkedList
   */
  public shift(): T {
    if (this.isEmpty()) {
      throw new Error("List is empty.");
    }

    let value = this._head.value!;

    this._head = this._head.next!;
    if (this._head) {
      this._head.prev = null;
    }

    this._length--;

    return value;
  }

  /**
   * Prepends the doubly linked list with an element.
   *
   * @param {T} value
   * @memberof DoubleLinkedList
   */
  public unshift(value: T): void {
    // allocate new node
    const node = new DS.DoubleNode<T>(value, null, this._head);

    if (this.isEmpty()) {
      this._head = this._tail = node;
    } else {
      this._head.prev = node;
      this._head = this._head.prev;
    }

    this._length++;
  }

  get length(): number {
    return this._length;
  }
}

export class LinkedList<T> {
  private _head: DS.Node<T>;
  private _tail: DS.Node<T>;

  public constructor() {
    this._head = new DS.Node<T>();
    this._tail = new DS.Node<T>();
    this._head.next = this._tail;
  }

  public isEmpty(): boolean {
    return this._head.next === this._tail;
  }

  public insertFirst(item: T): void {
    const node = new DS.Node<T>(item);

    node.next = this._head.next;
    this._head.next = node;
  }

  public insertLast(item: T): void {
    const node = new DS.Node<T>(item);

    let current: DS.Node<T> | null = this._head;

    while (current && current.next !== this._tail) {
      current = current.next;
    }

    if (current) {
      node.next = this._tail;
      current.next = node;
    }
  }

  public removeFirst(): T | null {
    if (this.isEmpty()) {
      throw new Error("List is empty");
    }

    let rv: DS.Node<T> | null = this._head.next;

    if (rv) {
      this._head.next = rv.next;
      rv.next = null;
    }

    // We are returning the data, not the node itself
    return rv ? rv.item : null;
  }

  public remove(searchKey: T): T | null {
    if (this.isEmpty()) {
      throw new Error("List is empty");
    }

    let result: DS.Node<T> | null = null;

    let current: DS.Node<T> = this._head;

    while (current.next && current.next.item !== searchKey) {
      current = current.next;
    }

    if (current.next) {
      result = current.next;
      current.next = current.next.next;
      result.next = null;
    }

    return result && result.item ? result.item : null;
  }

  public contains(searchItem: T): boolean {
    if (this.isEmpty()) {
      throw new Error("List is empty");
    }

    let rv: boolean = false;
    let cur: DS.Node<T> | null = this._head;

    // Traverse the list in search of a matching item
    while (cur && cur.next !== this._tail) {
      if (cur.next && cur.next.item === searchItem) {
        rv = true;
        break;
      }
      cur = cur.next;
    }

    return rv;
  }

  public getFirst(): T | null {
    if (this.isEmpty()) {
      throw new Error("List is empty");
    }

    return this._head.next ? this._head.next.item : null;
  }

  public listContents(): void {
    let current = this._head.next;

    while (current && current !== this._tail) {
      current = current.next;
    }
  }
}
