import { HexCoord } from "./HexCoord";

export class HexMap<T> {
  private _hexDict: Map<string, T>;

  public static AreNeighbors(a: HexCoord, b: HexCoord): boolean {
    let diff = a.subtract(b).toArray();
    if (!diff.includes(0)) return false;
    if (!diff.includes(1)) return false;
    if (!diff.includes(-1)) return false;
    return true;
  }

  public static New<T>(): HexMap<T> {
    return new HexMap();
  }

  private constructor() {
    this._hexDict = new Map();
  }

  get hexDict(): Map<string, T> {
    return this._hexDict;
  }

  get size(): number {
    return this._hexDict.size;
  }

  public keys(): IterableIterator<string> {
    return this._hexDict.keys();
  }

  public add(coords: string, item: T): void {
    this._hexDict.set(coords, item);
  }

  public remove(coords: HexCoord): void {
    this._hexDict.delete(coords.toString());
  }

  public get(coords: HexCoord): T | undefined {
    return this._hexDict.get(coords.toString());
  }

  public exists(coordString: string): boolean {
    return this._hexDict.has(coordString);
  }

  public coordExists(coord: HexCoord): boolean {
    return this._hexDict.has(coord.toString());
  }

  public getWithString(coordString: string): T | undefined {
    let coord = HexCoord.FromString(coordString);
    return this.get(coord);
  }

  public merge(hexMap: HexMap<T>): this {
    for (let key of hexMap.hexDict.keys()) {
      this.add(key, hexMap.getWithString(key)!);
    }

    return this;
  }

  public toJSON(): {} {
    let json: { [hexCoord: string]: T } = {};

    const it = this._hexDict.keys();
    let result = it.next();

    while (!result.done) {
      json[result.value] = this._hexDict.get(result.value)!;
      result = it.next();
    }

    return json;
  }
}
