import { LinkedList } from "./LinkedList";
import type { Queue } from "./Queue";

export class StackList<T> {
  private _list: LinkedList<T>;

  public static CreateFromQueue<T>(queue: Queue<T>): StackList<T> {
    let result = new StackList<T>();
    for (let item of queue) {
      result.push(item);
    }

    return result;
  }

  public constructor() {
    this._list = new LinkedList<T>();
  }

  public isEmpty(): boolean {
    return this._list.isEmpty();
  }

  public push(item: T): void {
    this._list.insertFirst(item);
  }

  public pop(): T | null {
    return this._list.removeFirst();
  }

  public top(): T | null {
    return this._list.getFirst();
  }

  public stackContents(): void {
    this._list.listContents();
  }
}
