import { HexMap } from "../../Utils/HexMap";
import { HexCoord } from "../../Utils/HexCoord";
import type { GameTile } from "../GameTiles/GameTile";

export class Board {
  #tiles: HexMap<GameTile>;
  #generationStrategy: string;

  public static New(strategy: string): Board {
    return new Board(strategy);
  }

  private constructor(strategy: string) {
    this.#tiles = HexMap.New(); //this.createMap(this.createBiomeTypeStack());
    this.#generationStrategy = strategy;
  }

  public addTile(hexCoord: HexCoord, tile: GameTile): void {
    this.#tiles.add(hexCoord.toString(), tile);
  }

  get hexCoords(): IterableIterator<string> {
    return this.#tiles.hexDict.keys();
  }

  public getTile(coord: HexCoord): GameTile {
    const coordString = coord.toString();
    if (!this.#tiles.exists(coordString)) {
      throw new Error(`Board does not have the coordinates ${coord}.`);
    }
    return this.#tiles.getWithString(coordString)!;
  }

  public coordExists(coord: HexCoord): boolean {
    return this.#tiles.coordExists(coord);
  }

  public isApex(coord: HexCoord): boolean {
    let count = 0;
    if (this.#tiles.exists(coord.add(HexCoord.Create(-1, 1, 0)).toString())) {
      count++;
    }
    if (this.#tiles.exists(coord.add(HexCoord.Create(0, 1, -1)).toString())) {
      count++;
    }
    if (this.#tiles.exists(coord.add(HexCoord.Create(1, 0, -1)).toString())) {
      count++;
    }
    if (this.#tiles.exists(coord.add(HexCoord.Create(1, -1, 0)).toString())) {
      count++;
    }
    if (this.#tiles.exists(coord.add(HexCoord.Create(0, -1, 1)).toString())) {
      count++;
    }
    if (this.#tiles.exists(coord.add(HexCoord.Create(-1, 0, 1)).toString())) {
      count++;
    }

    return count === 3;
  }

  get strategy(): string {
    return this.#generationStrategy;
  }

  // get hexMap(): HexMap<Terrain> {
  //   return this.#tiles;
  // }
}
