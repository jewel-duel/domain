import type { Board } from "./Board";
import type { IBoardStrategy } from "./Strategies/IBoardStrategy";

export class BoardFactory {
  public static Create(strategy: IBoardStrategy): Board {
    return strategy.execute();
  }
}
