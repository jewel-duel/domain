import { Terrain } from "../Terrains/TerrainType";

/**
 * A collection of related terrain types.
 *
 * @export
 * @class Biome
 */
export class Biome {
  private _specialTile: Terrain;

  //#region Factories

  /**
   * Builds a desert biome. Uses the sand terrain type.
   *
   * @static
   * @returns {Biome}
   * @memberof Biome
   */
  public static Desert(): Biome {
    return new Biome(Terrain.Sand);
  }

  /**
   * Builds a swamp biome. Users the marsh terrain type.
   *
   * @static
   * @returns {Biome}
   * @memberof Biome
   */
  public static Swamp(): Biome {
    return new Biome(Terrain.Marsh);
  }

  /**
   * Builds the tundra biome. Uses the ice terrain type.
   *
   * @static
   * @returns {Biome}
   * @memberof Biome
   */
  public static Tundra(): Biome {
    return new Biome(Terrain.Ice);
  }

  //#endregion

  //#region Constructors

  /**
   * Creates an instance of Biome.
   * @param {Terrain} terrain
   * @memberof Biome
   */
  protected constructor(terrain: Terrain) {
    this._specialTile = terrain;
  }

  //#endregion

  //#region Getters

  /**
   * Returns the terrain type assigned to the biome.
   *
   * @readonly
   * @type {Terrain}
   * @memberof Biome
   */
  get specialTile(): Terrain {
    return this._specialTile;
  }

  //#endregion
}
