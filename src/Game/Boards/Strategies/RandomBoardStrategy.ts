import { Board } from "../Board";
import { HexCoord } from "../../../Utils/HexCoord";
import { GameTile } from "../../GameTiles/GameTile";
import { TerrainType } from "../Terrains/TerrainType";
import { BaseBoardStrategy } from "./BaseBoardStrategy";
import { StrategyType } from "./StrategyFactory";

export interface IRandomBoardStrategyOptions {
  size: number;
}

export class RandomBoardStrategy extends BaseBoardStrategy {
  private static _terrainTypeArray: string[] = TerrainType.PlaceableTypes();

  private static _randomTerrainType(): string {
    return RandomBoardStrategy
      ._terrainTypeArray[
        Math.floor(Math.random() * RandomBoardStrategy._terrainTypeArray.length)
      ];
  }

  private _options: IRandomBoardStrategyOptions;

  public constructor(options: IRandomBoardStrategyOptions = { size: 5 }) {
    super(StrategyType.Random);
    if (options.size < 2) {
      throw new Error(
        `A board must have a larger radius than ${options.size} tiles.`,
      );
    }

    this._options = options;
  }

  get options(): IRandomBoardStrategyOptions {
    return this._options;
  }

  public execute(): Board {
    const board = Board.New(this.type);
    const size = this._options.size;

    // Add random tiles
    for (let q = -size; q <= size; q++) {
      let r1 = Math.max(-size, -q - size);
      let r2 = Math.min(size, -q + size);
      for (let r = r1; r <= r2; r++) {
        let terrain = RandomBoardStrategy._randomTerrainType();
        board.addTile(
          HexCoord.Create(q, r, -q - r),
          GameTile.Unexplored(TerrainType.FromString(terrain)),
        );
      }
    }

    // Add volcanos
    board.addTile(HexCoord.Origin(), GameTile.Explored("volcano"));
    // Add Gem Temples
    board.addTile(
      HexCoord.Create(0, -size, size),
      GameTile.Explored("gemTemple"),
    );
    board.addTile(
      HexCoord.Create(-size, size, 0),
      GameTile.Explored("gemTemple"),
    );
    board.addTile(
      HexCoord.Create(size, 0, -size),
      GameTile.Explored("gemTemple"),
    );
    board.addTile(
      HexCoord.Create(0, size, -size),
      GameTile.Explored("gemTemple"),
    );
    board.addTile(
      HexCoord.Create(size, -size, 0),
      GameTile.Explored("gemTemple"),
    );
    board.addTile(
      HexCoord.Create(-size, 0, size),
      GameTile.Explored("gemTemple"),
    );

    return board;
  }
}
