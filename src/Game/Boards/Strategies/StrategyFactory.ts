import type { IBoardStrategy } from "./IBoardStrategy";
import {
  BiomeBoardStrategy,
  IBiomeBoardStrategyOptions,
} from "./BiomeBoardStrategy";
import {
  IRandomBoardStrategyOptions,
  RandomBoardStrategy,
} from "./RandomBoardStrategy";

export class StrategyFactory {
  public static Create(
    strategy: string = "random",
    options?: {},
  ): IBoardStrategy {
    let result: IBoardStrategy;
    if (strategy === "biome") {
      result = new BiomeBoardStrategy(options as IBiomeBoardStrategyOptions);
    } else {
      result = new RandomBoardStrategy(options as IRandomBoardStrategyOptions);
    }

    return result;
  }
}

export enum StrategyType {
  Biome = "biome",
  Random = "random",
}
