import type { Board } from "../Board";
import type { StrategyType } from "./StrategyFactory";

export interface IBoardStrategy {
  type: StrategyType;
  execute(): Board;
}
