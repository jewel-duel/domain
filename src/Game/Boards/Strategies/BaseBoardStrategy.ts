import type { IBoardStrategy } from "./IBoardStrategy";
import type { Board } from "../Board";
import type { StrategyType } from "./StrategyFactory";

export abstract class BaseBoardStrategy implements IBoardStrategy {
  protected _type: StrategyType;
  abstract execute(): Board;

  constructor(type: StrategyType) {
    this._type = type;
  }

  get type(): StrategyType {
    return this._type;
  }
}
