/**
 * Tracks player actions during their turn.
 *
 * @export
 * @class PlayerActions
 */
export class PlayerActions {
  #actionsCount!: number;
  #capacity: number;

  /**
   * Static method to create a PlayerActions class with capacity of 3 actions.
   *
   * @static
   * @returns {PlayerActions}
   * @memberof PlayerActions
   */
  public static Default(): PlayerActions {
    return new PlayerActions(3, 3);
  }

  public static DefaultEmpty(): PlayerActions {
    return new PlayerActions(3, 0);
  }

  public static WithCapacity(capacity: number): PlayerActions {
    return new PlayerActions(capacity);
  }

  public static EmptyWithCapacity(capacity: number): PlayerActions {
    return new PlayerActions(capacity, 0);
  }

  private constructor(capacity: number, actions?: number) {
    this.#capacity = capacity;
    if (actions || actions === 0) {
      if (actions > capacity) {
        throw new Error("Actions cannot be greater than capacity.");
      }
      this.#actionsCount = actions;
    } else {
      this.resetCount();
    }
  }

  /**
   * Resets the number of available actions back to the capacity.
   *
   * @memberof PlayerActions
   */
  public resetCount(): void {
    this.#actionsCount = this.#capacity;
  }

  get actions(): number {
    return this.#actionsCount;
  }

  set actions(actions: number) {
    if (actions > this.#capacity) {
      throw new Error("Exceeds capacity for actions.");
    }
    this.#actionsCount = actions;
  }

  get capacity(): number {
    return this.#capacity;
  }

  get isFull(): boolean {
    return this.#actionsCount === this.#capacity;
  }

  get isEmpty(): boolean {
    return this.#actionsCount === 0;
  }

  public spendAction(amount: number = 1): void {
    if (amount < 0) {
      throw new Error("Amount must be greater than zero.");
    }
    if (this.#actionsCount - amount < 0) {
      throw new Error("Not enough actions to spend.");
    }

    this.#actionsCount -= amount;
  }

  public fill(): void {
    this.#actionsCount = this.#capacity;
  }

  public empty(): void {
    this.#actionsCount = 0;
  }
}
