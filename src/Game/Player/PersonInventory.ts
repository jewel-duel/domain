import type { Wieldable } from "../Items/Wieldable";
import type { Wearable } from "../Items/Wearable";
import type { Item } from "../Items/Item";
import { Category } from "../Items/Category";
import { PutInventoryError } from "./PutInventoryError";
import {
  Slot,
  SlotFactory,
  SlotKeys,
  WearableSlot,
  WieldableSlot,
} from "./Slot";

interface IPersonSlo {
  "left_hand": WieldableSlot;
  "right_hand": WieldableSlot;
  "head": WearableSlot;
  "left_arm": WearableSlot;
  "body": WearableSlot;
  "right_arm": WearableSlot;
  "left_leg": WearableSlot;
  "right_leg": WearableSlot;
}

export type PersonKeys = keyof IPersonSlo;
export enum WieldableKeys {
  "left_hand",
  "right_hand",
}

export enum WearableKeys {
  "head",
  "left_arm",
  "body",
  "right_arm",
  "left_leg",
  "right_leg",
}

export class PersonInventory {
  #stuff: { [key: string]: WearableSlot | WieldableSlot } = {};

  public static Default(): PersonInventory {
    return new PersonInventory();
  }

  private constructor() {
    let slot: Slot;
    slot = SlotFactory.WieldableSlot(SlotKeys.LeftHand.toString());
    this.#stuff[slot.key] = slot;
    slot = SlotFactory.WieldableSlot(SlotKeys.RightHand.toString());
    this.#stuff[slot.key] = slot;
    slot = SlotFactory.WearableSlot(SlotKeys.Head.toString());
    this.#stuff[slot.key] = slot;
    slot = SlotFactory.WearableSlot(SlotKeys.LeftArm.toString());
    this.#stuff[slot.key] = slot;
    slot = SlotFactory.WearableSlot(SlotKeys.Body.toString());
    this.#stuff[slot.key] = slot;
    slot = SlotFactory.WearableSlot(SlotKeys.RightArm.toString());
    this.#stuff[slot.key] = slot;
    slot = SlotFactory.WearableSlot(SlotKeys.LeftLeg.toString());
    this.#stuff[slot.key] = slot;
    slot = SlotFactory.WearableSlot(SlotKeys.RightLeg.toString());
    this.#stuff[slot.key] = slot;
  }

  public put(item: Wearable | Wieldable, slotKey: SlotKeys): Item | null {
    if (
      slotKey.toLowerCase() in WieldableKeys &&
      item.category === Category.Wearables
    ) {
      throw new PutInventoryError(
        item.category.toString(),
        Category.Wieldables.toString(),
      );
    }
    if (
      slotKey.toLowerCase() in WearableKeys &&
      item.category === Category.Wieldables
    ) {
      throw new PutInventoryError(
        item.category.toString(),
        Category.Wieldables.toString(),
      );
    }
    return this.#stuff[slotKey.toString()].put(item);
  }

  public get(slotKey: SlotKeys): WearableSlot | WieldableSlot {
    const slotKeyString = slotKey.toString();
    const slot = this.#stuff[slotKeyString];
    return slot;
  }

  public clear(slotKey: SlotKeys): Item | null {
    return this.#stuff[slotKey.toString()].clear();
  }

  get slo() : {[key: string]: WearableSlot | WieldableSlot } {
    return this.#stuff;
  }
}
