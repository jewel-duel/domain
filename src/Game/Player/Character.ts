import { HexCoord } from "../../Utils/HexCoord";
import { PlayerActions } from "./PlayerActions";
import type { Color } from "../Colors/Color";
import { Inventory } from "./Inventory";

export class Character {
  #actions: PlayerActions;
  #position: HexCoord;
  #color: Color;
  #inventory: Inventory;

  public constructor(color: Color) {
    this.#actions = PlayerActions.Default();
    this.#position = HexCoord.Origin();
    this.#color = color;
    this.#inventory = Inventory.Default();
  }

  get color(): Color {
    return this.#color;
  }

  get actions(): PlayerActions {
    return this.#actions;
  }

  get position(): HexCoord {
    return this.#position;
  }

  get inventory(): Inventory {
    return this.#inventory;
  }

  set position(coord: HexCoord) {
    this.#position = coord;
  }
}
