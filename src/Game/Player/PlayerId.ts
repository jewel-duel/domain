import { generate } from "../../Utils/v4";

export class PlayerId {
  #value: string;

  public static Create() {
    return new PlayerId(generate());
  }

  public constructor(value: string) {
    this.#value = value;
  }

  get value(): string {
    return this.#value;
  }

  public toString(): string {
    return this.#value;
  }
}
