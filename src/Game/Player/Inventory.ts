import type { Item } from "../Items/Item";
import { Backpack } from "./Backpack";
import { PersonInventory } from "./PersonInventory";
import {
  BackpackSlot,
  SlotKeyType,
  WearableSlot,
  WieldableSlot,
} from "./Slot";

export class Inventory {
  #backpack: Backpack;
  #personal: PersonInventory;

  public static Default(): Inventory {
    return new Inventory();
  }

  private constructor() {
    this.#backpack = Backpack.Create();
    this.#personal = PersonInventory.Default();
  }

  public put(item: Item | null, slotKey: string): Item | null {
    return this.get(slotKey).put(item);
  }

  public swap(firstKey: string, secondKey: string) {
    const firstSlot = this.get(firstKey);
    const secondSlot = this.get(secondKey);

    const firstSlotItem = firstSlot.contents;
    firstSlot.put(secondSlot.put(firstSlotItem));
  }

  public get(slotKey: string): BackpackSlot | WearableSlot | WieldableSlot {
    const bpi = this.backPackIndexCheck(slotKey);
    if (bpi >= 0) {
      return this.#backpack.get(bpi);
    } else {
      const slot = this.#personal.get(SlotKeyType.FromString(slotKey));
      return slot;
    }
  }

  private backPackIndexCheck(slotKey: string): number {
    let index = -1;
    const slotkey = slotKey.toLowerCase();
    if (slotkey.includes("backpack")) {
      const key = parseInt(slotkey.replace("backpack", ""));
      index = key - 1;
    }
    return index;
  }

  get backpack(): Backpack {
    return this.#backpack;
  }

  get personal(): PersonInventory {
    return this.#personal;
  }
}
