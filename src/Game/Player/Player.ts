import type { HexCoord } from "../../Utils/HexCoord";
import type { Color } from "../Colors/Color";
import { Character } from "./Character";
import type { PlayerActions } from "./PlayerActions";
import { PlayerId } from "./PlayerId";

export class Player {
  #name: string;
  #id: PlayerId;
  #connected: boolean;
  #character: Character;

  //#region Factories

  /**
   * Creates a player from a User object.
   *
   * @static
   * @param {string} userName
   * @param {string} userId
   * @param {Color} color
   * @returns {Player}
   * @memberof Player
   */
  public static FromUser(
    userName: string,
    userId: string,
    color: Color,
  ): Player {
    return new Player(userName, new PlayerId(userId), color, true);
  }

  /**
   * Creates a player.
   *
   * @static
   * @param {string} name
   * @param {PlayerId} id
   * @param {Color} color
   * @param {boolean} connected
   * @returns {Player}
   * @memberof Player
   */
  public static Create(
    name: string,
    id: PlayerId,
    color: Color,
    connected: boolean,
  ): Player {
    return new Player(name, id, color, connected);
  }

  //#endregion

  private constructor(
    name: string,
    id: PlayerId,
    color: Color,
    connected: boolean,
  ) {
    this.#name = name;
    this.#id = id;
    this.#connected = connected;
    this.#character = new Character(color);
  }

  /**
   * The Id of the player.
   *
   * @readonly
   * @type {PlayerId}
   * @memberof Player
   */
  get id(): PlayerId {
    return this.#id;
  }

  /**
   * The name of the player.
   *
   * @readonly
   * @type {string}
   * @memberof Player
   */
  get name(): string {
    return this.#name;
  }

  /**
   * The color of the player.
   *
   * @readonly
   * @type {Color}
   * @memberof Player
   */
  get color(): Color {
    return this.#character.color;
  }

  /**
   * The current position of the player piece.
   *
   * @readonly
   * @type {HexCoord}
   * @memberof Player
   */
  get position(): HexCoord {
    return this.#character.position;
  }

  /**
   * The number of actions left in a turn.
   *
   * @readonly
   * @type {PlayerActions}
   * @memberof Player
   */
  get actions(): PlayerActions {
    return this.#character.actions;
  }

  /**
   * The character (board piece) the player is playing.
   */
  get character(): Character {
    return this.#character;
  }

  /**
   * Whether the player is connected or not.
   *
   * @readonly
   * @type {boolean}
   * @memberof Player
   */
  get isConnected(): boolean {
    return this.#connected;
  }

  /**
   * Moves the player to a position.
   *
   * @param {HexCoord} dest
   * @memberof Player
   */
  public moveTo(dest: HexCoord) {
    this.#character.position = dest;
  }

  /**
   * Marks a player as connected.
   *
   * @memberof Player
   */
  public connect(): void {
    this.#connected = true;
  }

  /**
   * Marks a player as disconnected.
   *
   * @memberof Player
   */
  public disconnect(): void {
    this.#connected = false;
  }
}
