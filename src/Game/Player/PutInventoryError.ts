export class PutInventoryError extends Error {
    itemCategory: string;
    slotKey: string;
    constructor(itemCategory: string, slotKey: string, ...params: (string | undefined)[]) {
      // Pass remaining arguments (including vendor specific ones) to parent constructor
      super(...params)
  
      // Maintains proper stack trace for where our error was thrown (only available on V8)
      if (Error.captureStackTrace) {
        Error.captureStackTrace(this, PutInventoryError)
      }
  
      this.name = 'CustomError'
      // Custom debugging information
      this.itemCategory = itemCategory
      this.slotKey = slotKey
      this.message = `Item of category ${itemCategory} cannot be placed in slot of category ${slotKey}`
    }
  }