import generateRandomString from "../Utils/RandomString";
import type { Player } from "./Player/Player";
import type { Board } from "./Boards/Board";
import type { IBoardStrategy } from "./Boards/Strategies/IBoardStrategy";
import { BoardFactory } from "./Boards/BoardFactory";
import { StrategyFactory } from "./Boards/Strategies/StrategyFactory";
import { TurnManager } from "./TurnManager";
import type { HexCoord } from "../Utils/HexCoord";
import { HexMap } from "../Utils/HexMap";

export interface IGameOptions {
  key: string;
  players: Player[];
  strategy: IBoardStrategy;
}

/**
 * The Options for starting  a game
 *
 * @export
 * @class GameOptions
 * @implements {IGameOptions}
 */
export class GameOptions implements IGameOptions {
  #key: string;
  #players: Player[];
  #strategy: IBoardStrategy;

  constructor(
    key: string,
    players: Player[],
    strategy: IBoardStrategy = StrategyFactory.Create("random"),
  ) {
    this.#key = key;
    this.#strategy = strategy;
    this.#players = players;
  }

  /**
   * The unique identifier for the game
   *
   * @readonly
   * @type {string}
   * @memberof GameOptions
   */
  get key(): string {
    return this.#key;
  }

  /**
   * The player who will play the game
   *
   * @readonly
   * @type {Player[]}
   * @memberof GameOptions
   */
  get players(): Player[] {
    return this.#players;
  }

  /**
   * The board generation strategy
   *
   * @readonly
   * @type {IBoardStrategy}
   * @memberof GameOptions
   */
  get strategy(): IBoardStrategy {
    return this.#strategy;
  }
}

/**
 * The state of the game.
 *
 * Players can perform actions on this state.
 * The object contains logic to determine if the
 * actions are legal or not and what the
 * consequences of those actions are on the
 * rest of the game state.
 *
 * @export
 * @class Game
 */
export class Game {
  private _started: boolean = false;
  private _key: string;
  private _players: Player[];
  private _board: Board | null = null;
  private _creationDate: number;
  private _turnManager: TurnManager;

  //#region Factories

  public static Create(options: IGameOptions): Game {
    return new Game(options);
  }

  //#endregion

  /**
   * Creates an instance of Game.
   * @memberof Game
   * @constructor
   */
  private constructor(options: IGameOptions) {
    this._key = options.key || generateRandomString();
    this._players = [];

    // TODO: validate players length
    options.players.forEach((player) => {
      this._addPlayer(player);
    });

    if (this._players.length > 0) {
      this._board = BoardFactory.Create(options.strategy);
    }

    this._turnManager = new TurnManager(this._players);

    this._creationDate = Date.now();
    this._started = true;
  }

  /**
   * Generates a board and sets the start flag to true.
   *
   * @param generationStrategy
   */
  public start(generationStrategy?: IBoardStrategy): void {
    generationStrategy = generationStrategy || StrategyFactory.Create("random");
    if (this._players.length > 0) {
      this._board = BoardFactory.Create(generationStrategy!);
      this._started = true;
    }
  }

  private _addPlayer(player: Player): void {
    if (!this._started) this._players.push(player);
    // TODO: throw error if game has already started.
  }

  //#region Getters

  /**
   * The key of the Game.
   *
   * @readonly
   * @type {string}
   * @memberof Game
   */
  get key(): string {
    return this._key;
  }

  /**
   * Whether the game has started or not.
   *
   * @readonly
   * @type {boolean}
   * @memberof Game
   */
  get started(): boolean {
    return this._started;
  }

  /**
   * The players in the game.
   *
   * @readonly
   * @type {Player[]}
   * @memberof Game
   */
  get players(): Player[] {
    return this._players;
  }

  /**
   * Retrieves a player by it's id string.
   *
   * @param {string} playerId
   * @returns {(Player | null)}
   * @memberof Game
   */
  public getPlayerById(playerId: string): Player | null {
    const result = this._players.find((x) => x.id.toString() === playerId);
    return !!result ? result : null;
  }

  /**
   * Marks a player as disconnected.
   *
   * @param {Player} player
   * @memberof Game
   */
  public disconnectPlayer(player: Player): void {
    const playerIdString = player.id.toString();
    const p = this._players.find((x) => x.id.toString() === playerIdString);
    if (p) {
      p.disconnect();
    }
  }

  /**
   * Determines if any players are connected
   *
   * @returns {boolean} - True if any player is connected, false if all of them are disconnected.
   * @memberof Game
   */
  public playersConnected(): boolean {
    const result = this._players.find((x) => x.isConnected);
    return result ? true : false;
  }

  /**
   * The Game Board.
   *
   * @readonly
   * @type {(Board | null)}
   * @memberof Game
   */
  get board(): Board | null {
    return this._board;
  }

  /**
   * The epoch time the game was created.
   *
   * @readonly
   * @type {number}
   * @memberof Game
   */
  get creationDate(): number {
    return this._creationDate;
  }

  /**
   * The Turn Manager of the Game.
   *
   * @readonly
   * @type {TurnManager}
   * @memberof Game
   */
  get turnManager(): TurnManager {
    return this._turnManager;
  }

  // Methods

  /**
   * Uses the turn manager to move the initiative to the next player.
   *
   * @memberof Game
   */
  public nextTurn(): void {
    this._turnManager.next();
    this._turnManager.currentPlayer.actions.resetCount();
  }

  /**
   * Moves a player on the board.
   *
   * @param {Player} player
   * @param {HexCoord} dest
   * @returns {void}
   * @memberof Game
   */
  public movePlayer(player: Player, dest: HexCoord): void {
    if (this._board == null) return;
    if (player.id != this._turnManager.currentPlayer.id) {
      throw new Error("Player cannot move out of turn.");
    }
    if (!this._board.coordExists(dest)) {
      throw new Error("Tile does not exist.");
    }
    if (!HexMap.AreNeighbors(player.position, dest)) {
      throw new Error("Can only move to adjacent tile.");
    }
    if (player.actions.isEmpty) {
      throw new Error("Player out of actions.");
    }

    // Calculate actions needed to move.
    let actions_required = 1;
    const dest_tile = this._board.getTile(dest);
    const start_tile = this._board.getTile(player.position);
    const elevation_diff = dest_tile.elevation - start_tile.elevation;
    if (elevation_diff > 0) {
      actions_required = actions_required + elevation_diff;
    }

    if (player.actions.actions < actions_required) {
      throw new Error("Not enough actions to move to tile");
    }

    player.moveTo(dest);
    player.actions.spendAction(actions_required);
  }

  //#endregion
}
