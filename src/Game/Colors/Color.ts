export class Color {
  #color: string;

  public static Green(): Color {
    return new Color("green");
  }

  public static Blue(): Color {
    return new Color("blue");
  }

  public static Red(): Color {
    return new Color("red");
  }

  public static Yellow(): Color {
    return new Color("yellow");
  }

  public static Cyan(): Color {
    return new Color("cyan");
  }

  public static Magenta(): Color {
    return new Color("magenta");
  }

  public static Purple(): Color {
    return new Color("purple");
  }

  public static Orange(): Color {
    return new Color("orange");
  }

  public static White(): Color {
    return new Color("white");
  }

  public static Black(): Color {
    return new Color("black");
  }

  public static FromString(color: string): Color {
    switch (color) {
      case "black":
        return Color.Black();
        break;
      case "white":
        return Color.White();
        break;
      case "orange":
        return Color.Orange();
        break;
      case "purple":
        return Color.Purple();
        break;
      case "magenta":
        return Color.Magenta();
        break;
      case "cyan":
        return Color.Cyan();
        break;
      case "yellow":
        return Color.Yellow();
        break;
      case "red":
        return Color.Red();
        break;
      case "blue":
        return Color.Blue();
        break;
      case "green":
        return Color.Green();
        break;
      default:
        throw new Error(`Color value of ${color} is invalid.`);
        break;
    }
  }

  private constructor(color: string) {
    this.#color = color;
  }

  get color(): string {
    return this.#color;
  }
}
