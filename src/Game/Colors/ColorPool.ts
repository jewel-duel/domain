import { ShuffleableQueue } from "../../Utils/ShuffleableQueue";
import type { Color } from "./Color";

export class ColorPool {
  #choices: ShuffleableQueue<Color>;

  public constructor(choices: Color[] = []) {
    this.#choices = new ShuffleableQueue<Color>(choices.length);

    choices.forEach((choice) => {
      this.#choices.enqueue(choice);
    });

    this.#choices.shuffle();
  }

  public get(): Color {
    if (this.#choices.isEmpty()) {
      throw new Error("The pool is empty.");
    }
    return this.#choices.dequeue();
  }

  public shuffle(): void {
    this.#choices.shuffle();
  }

  public release(color: Color): void {
    this.#choices.enqueue(color);
    this.shuffle();
  }
}
