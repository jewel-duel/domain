import type { Category } from "./Category";
import { Item } from "./Item";
import type { Kind } from "./Kind";
import type { Resources } from "./Resources";

export class Craftable extends Item {
    #type : Resources
    #materials : Resources[]
    #price: number
  
    protected constructor(id: string, kind: Kind, category: Category, type: Resources, materials: Resources[], price:number) {
      super(id, kind, category);
      this.#type = type;
      this.#materials = materials;
      this.#price = price;
    }

    get type() : Resources {
        return this.#type;
    }

    get materials() : Resources[] {
        return this.#materials
    }

    get price() : number {
        return this.#price
    }
  }