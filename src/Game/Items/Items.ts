import { generate } from "../../Utils/v4";
import { Category } from "./Category";
import { Item } from "./Item";
import { Kind } from "./Kind";
import type { Resources } from "./Resources";
import { Wearable } from "./Wearable";
import { Wieldable } from "./Wieldable";

export class ItemFactory {
  public static Armor(type: Resources): Wearable {
    return Wearable.Armor(type);
  }

  public static Shield(type: Resources): Wieldable {
    return Wieldable.Shield(type);
  }

  public static Sword(type: Resources): Wieldable {
    return Wieldable.Sword(type);
  }

  public static Bow(type: Resources): Wieldable {
    return Wieldable.Bow(type);
  }

  public static Resource(type: Resources): Item {
    const id = generate();
    return new Item(id, type, Category.Resource);
  }

  public static Gold(): Item {
    const id = generate();
    return new Item(id, Kind.Gold, Category.Specials);
  }

  public static Duck(): Item {
    const id = generate();
    return new Item(id, Kind.Duck, Category.Specials);
  }

  public static Gem(): Item {
    const id = generate();
    return new Item(id, Kind.Gem, Category.Specials);
  }

  public static Wood(): Item {
    const id = generate();
    return new Item(id, Kind.Wood, Category.Resource);
  }

  public static Iron(): Item {
    const id = generate();
    return new Item(id, Kind.Iron, Category.Resource);
  }

  public static Bronze(): Item {
    const id = generate();
    return new Item(id, Kind.Bronze, Category.Resource);
  }
}
