import type { Category } from "./Category";
import type { Kind } from "./Kind";

export class Item {
  #id: string;
  #kind: Kind;
  #category: Category;

  public constructor(id: string, kind: Kind, category: Category) {
    this.#id = id;
    this.#kind = kind;
    this.#category = category;
  }

  get id(): string {
    return this.#id;
  }

  get kind(): Kind {
    return this.#kind;
  }

  get category(): Category {
    return this.#category;
  }
}
