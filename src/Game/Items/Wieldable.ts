import { generate } from "../../Utils/v4";
import { Category } from "./Category";
import { Craftable } from "./Craftable";
import { Kind } from "./Kind";
import type { Resources } from "./Resources";

export class Wieldable extends Craftable {
  #handedness = 1

  public static Sword(type: Resources): Wieldable {
    const id = generate();
    return new Wieldable(id, Kind.Sword, type, [type], 1, 1);
  }

  public static Bow(type: Resources): Wieldable {
    const id = generate();
    return new Wieldable(id, Kind.Bow, type, [type], 1, 2);
  }

  public static Shield(type: Resources) : Wieldable {
    const id = generate();
    return new Wieldable(id, Kind.Shield, type, [type], 1, 1);
  }

  protected constructor(id: string, kind: Kind, type: Resources, materials: Resources[], price: number, handedness: number) {
    super(id, kind, Category.Wieldables, type, materials, price);
    this.#handedness = handedness;
  }

  get handedness(): number {
    return this.#handedness;
  }
}
