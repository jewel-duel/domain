import { Category } from "./Category";
import { generate } from "../../Utils/v4";
import type { Resources } from "./Resources";
import { Craftable } from "./Craftable";
import { Kind } from "./Kind";

export class Wearable extends Craftable {

  public static Armor(type: Resources): Wearable {
    const id = generate();
    return new Wearable(id, Kind.Armor, type, [type], 1);
  }

  protected constructor(id: string, kind: Kind, type: Resources, materials: Resources[], price: number) {
    super(id, kind, Category.Wearables, type, materials, price);
  }
}
