import { generate } from "../Utils/v4";

export class User {
  name: string;
  id: string;

  constructor(name: string, id: string = generate()) {
    this.name = name;
    this.id = id;
  }
}
