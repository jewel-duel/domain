import { RandomBoardStrategy, HexCoord } from "../src";
import { BoardFactory } from "../src";

test("default board size should be 5", () => {
  let strategy = new RandomBoardStrategy();
  expect(strategy.options.size).toBe(5);
});

test("random board generated should be valid", () => {
  let strategy = new RandomBoardStrategy();
  let board = BoardFactory.Create(strategy);
  let volcanoCount = 0;
  let gemCount = 0;
  for (const item of board.hexCoords) {
    let tile = board.getTile(HexCoord.FromString(item));
    if (tile.type === "volcano") {
      expect(item).toBe(HexCoord.Origin().toString());
      volcanoCount++;
    }
    if (tile.type === "gemTemple") {
      expect(board.isApex(HexCoord.FromString(item))).toBe(true);
      gemCount++;
    }
  }

  expect(volcanoCount).toBe(1);
  expect(gemCount).toBe(6);
});
