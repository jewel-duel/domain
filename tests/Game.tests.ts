import { Game, GameOptions } from "../src/Game/Game";
import { Player } from "../src/Game/Player/Player";
import generateRandomString from "../src/Utils/RandomString";
import { Color } from "../src/Game/Colors/Color";
import { PlayerId } from "../src/Game/Player/PlayerId";

test("key should be set", () => {
  let randomString = generateRandomString();
  let player1 = Player.Create("player1", PlayerId.Create(), Color.Blue(), true);
  let options = new GameOptions(randomString, [player1]);
  let game = Game.Create(options);
  expect(game.key).toBe(randomString);
});
