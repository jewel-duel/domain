import { StrategyFactory } from "../src";

test("default strategy should be 'random'", () => {
  let strategy = StrategyFactory.Create();
  expect(strategy.type).toBe("random");
});
