import { TurnManager } from "../src/Game/TurnManager";
import { Player } from "../src/Game/Player/Player";
import { Color } from "../src/Game/Colors/Color";
import { PlayerId } from "../src/Game/Player/PlayerId";

let players: Player[];
let player1: Player;
let player2: Player;
let player3: Player;

beforeEach(() => {
  player1 = Player.Create("player1", PlayerId.Create(), Color.Blue(), true);
  player2 = Player.Create("player2", PlayerId.Create(), Color.Green(), true);
  player3 = Player.Create("player3", PlayerId.Create(), Color.Red(), true);
  players = [];
  players.push(player1);
  players.push(player2);
  players.push(player3);
});

test("first player in queue is first player to play", () => {
  let turnManager = new TurnManager(players);

  expect(turnManager.currentPlayer.id).toBe(player1.id);
});

test("second player goes second", () => {
  let turnManager = new TurnManager(players);
  turnManager.next();

  expect(turnManager.currentPlayer.id).toBe(player2.id);
});

test("turn count increments after each player has gone", () => {
  let turnManager = new TurnManager(players);

  turnManager.next();
  turnManager.next();

  expect(turnManager.turn).toBe(1);
  turnManager.next();
  expect(turnManager.turn).toBe(2);
});

test("last player goes twice between turns 1 and 2", () => {
  let turnManager = new TurnManager(players);

  turnManager.next();
  turnManager.next();

  expect(turnManager.currentPlayer.id).toBe(player3.id);

  turnManager.next();

  expect(turnManager.currentPlayer.id).toBe(player3.id);
});

test("player order should be swapped after first turn", () => {
  let turnManager = new TurnManager(players);

  expect(turnManager.isForward).toBe(true);

  turnManager.next();
  turnManager.next();
  turnManager.next();

  expect(turnManager.isForward).toBe(false);
});
