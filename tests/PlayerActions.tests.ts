import { PlayerActions } from "../src/Game/Player/PlayerActions";

test("default should have capacity of three actions", () => {
  let playerActions = PlayerActions.Default();

  expect(playerActions.capacity).toBe(3);
});

test("setting actions should affect the action count", () => {
  let playerActions = PlayerActions.Default();
  playerActions.actions = 2;

  expect(playerActions.actions).toBe(2);
});

test("spending too many actions should throw error", () => {
  let playerActions = PlayerActions.Default();
  playerActions.spendAction();
  playerActions.spendAction();
  playerActions.spendAction();

  expect(() => {
    playerActions.spendAction();
  }).toThrowError();
});

test("spending negative action amount should throw error", () => {
  let playerActions = PlayerActions.Default();
  expect(() => {
    playerActions.spendAction(-1);
  }).toThrowError();
});

test("spending too many actions at once should throw error", () => {
  let playerActions = PlayerActions.Default();
  expect(() => {
    playerActions.spendAction(4);
  }).toThrowError();
});

test("setting actions beyond capacity should throw error", () => {
  let playerActions = PlayerActions.Default();
  expect(() => {
    playerActions.actions = 4;
  }).toThrowError();
});

test("fill should fill actions to capacity", () => {
  let playerActions = PlayerActions.DefaultEmpty();
  playerActions.fill();
  expect(playerActions.isFull).toBe(true);
});

test("creating empty default playerActions should have no actions in it", () => {
  let playerActions = PlayerActions.DefaultEmpty();
  expect(playerActions.isEmpty).toBe(true);
});
