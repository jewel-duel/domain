import { CDLL } from "../src/Utils/CircularDoublyLinkedList";

test("second item inserted is second", () => {
  let num1 = 1;
  let num2 = 2;
  let list = new CDLL.CircularDoublyLinkedList<number>();
  list.insert(num1);
  list.insert(num2);

  //@ts-ignore
  let testNumber = list.proceed().data;

  if (testNumber != null) {
    expect(testNumber).toBe(num2);
  }
});
