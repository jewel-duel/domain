import { ColorPool } from "./Game/Colors/ColorPool.ts";
import { Color } from "./Game/Colors/Color.ts";
import type { Game } from "./Game/Game.ts";
import { Player } from "./Game/Player/Player.ts";
import type { User } from "./Users/User.ts";

export class Room {
  protected _game: Game | null = null;
  #players: Player[] = [];
  #colorPool: ColorPool;
  protected _name: string;

  constructor(name: string) {
    this._name = name;
    this.#colorPool = new ColorPool(
      [
        Color.Blue(),
        Color.Red(),
        Color.Green(),
        Color.Yellow(),
        Color.Purple(),
        Color.Orange(),
      ],
    );
  }

  addUser(user: User): Player {
    if (!this.userExists(user)) {
      const player = Player.FromUser(user.name, user.id, this.#colorPool.get());
      this.#players.push(player);
      return player;
    } else {
      return this.playerFromUserid(user.id)!;
    }
  }

  removeUser(user: User): void {
    const player = this.playerFromUserid(user.id);
    if (player) {
      if (this.gameStarted) {
        this.game!.disconnectPlayer(player);
      }
      this.#players = this.#players.filter((p: Player) => {
        return p.id.toString() !== user.id;
      });
      this.#colorPool.release(player.color);
    }
  }

  createGame(game: Game) {
    if (this._game === null) {
      this._game = game;
    }
  }

  get gameStarted(): boolean {
    return this._game !== null;
  }

  get game(): Game | null {
    return this._game;
  }

  get name(): string {
    return this._name;
  }

  get players(): Player[] {
    return this.#players;
  }

  get guests(): Player[] {
    if (this.#players.length > 1) {
      return this.#players.slice(1);
    }
    return [];
  }

  get host(): Player | null {
    if (this.#players.length > 0) {
      return this.#players[0];
    }
    return null;
  }

  public isEmpty(): boolean {
    return this.#players.length === 0;
  }

  public userExists(user: User): boolean {
    return this.#players.filter((p: Player) => {
      return user.id === p.id.toString();
    }).length > 0;
  }

  public playerFromUserid(userId: string): Player | null {
    const player = this.#players.filter((p: Player) => {
      return p.id.toString() === userId;
    });
    return player.length === 1 ? player[0] : null;
  }
}
