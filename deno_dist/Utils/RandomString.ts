const VALID_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const VALID_CHARACTERS_LENGTH = VALID_CHARACTERS.length;

/**
 * Generates a random string.
 *
 * @description The random string will consist of characters from this set: "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
 * 
 * @export
 * @param {number} [size=6] size - The size of the random string to generate.
 * @returns {string}
 */
export default function generateRandomString(size: number = 6): string {
  var result = "";

  for (let i = 0; i < size; i++) {
    result += VALID_CHARACTERS.charAt(
      Math.floor(Math.random() * VALID_CHARACTERS_LENGTH),
    );
  }

  return result;
}
