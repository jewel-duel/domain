export class Stack<T> {
  private _stack: T[];
  private _length: number;
  private readonly _maxSize: number;

  public constructor(maxSize: number) {
    this._length = 0;
    this._maxSize = maxSize;
    this._stack = new Array<T>(this._maxSize);
  }

  /**
   * Checks if the stack contains any elements.
   *
   * @returns {boolean} True if the stack is empty.
   * @memberof Stack
   */
  public isEmpty(): boolean {
    return this._length === 0;
  }

  public isFull(): boolean {
    return this._length === this._maxSize;
  }

  public push(item: T): void {
    if (this.isFull()) {
      throw new Error("Stack overflow");
    }

    this._stack[this._length++] = item;
  }

  public pop(): T {
    if (this.isEmpty()) {
      throw new Error("Stack empty");
    }

    const result = this._stack[--this._length];

    return result;
  }

  public top(): T {
    if (this.isEmpty()) {
      throw new Error("Stack empty");
    }

    return this._stack[this._length - 1];
  }

  public stackContents(): void {
    console.log("Stack contents");
    this._stack.forEach((item, index) => {
      console.log(`stack[${index}]: ${item}`);
    });
  }
}
