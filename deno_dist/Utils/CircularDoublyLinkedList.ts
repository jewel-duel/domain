export namespace CDLL {
  export class Node<T> {
    public constructor(
      public data: T | null = null,
      public next: Node<T> | null = null,
      public prev: Node<T> | null = null,
    ) {}
  }

  export class CircularDoublyLinkedList<T> {
    #start: Node<T> | null = null;
    #current: Node<T> | null = null;
    #length: number = 0;
    #forward: boolean = true;

    insert(value: T): void {
      let new_node = new Node(value);
      if (this.#start === null) {
        new_node.next = new_node;
        new_node.prev = new_node;
        this.#start = new_node;
        this.#current = new_node;
      } else {
        let last = this.#start.prev!;
        new_node.next = this.#start;
        this.#start.prev = new_node;
        new_node.prev = last;
        last.next = new_node;
      }
      this.#length++;
    }

    isEmpty(): boolean {
      return this.#length === 0;
    }

    get current(): Node<T> | null {
      return this.#current;
    }

    get length(): number {
      return this.#length;
    }

    public proceed(): Node<T> | null {
      if (this.isEmpty()) {
        return null;
      }

      if (this.#forward) {
        this.#current = this.#current!.next;
      } else {
        this.#current = this.#current!.prev;
      }

      return this.#current;
    }

    public peekForward(): Node<T> | null {
      const current = this.#current;
      if (current == null) return current;
      return current.next;
    }

    public isForward(): boolean {
      return this.#forward;
    }

    public isBackward(): boolean {
      return !this.#forward;
    }

    public reverse(): void {
      this.#forward = !this.#forward;
    }
  }
}
