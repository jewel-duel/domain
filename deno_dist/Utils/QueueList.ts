import { LinkedList } from "./LinkedList.ts";

export class QueueList<T> {
  private _list: LinkedList<T>;

  public constructor() {
    this._list = new LinkedList<T>();
  }

  public isEmpty(): boolean {
    return this._list.isEmpty();
  }

  public enqueue(item: T): void {
    this._list.insertLast(item);
  }

  public dequeue(): T | null {
    return this._list.removeFirst();
  }

  public peek(): T | null {
    return this._list.getFirst();
  }

  public queueContents(): void {
    this._list.listContents();
  }
}
