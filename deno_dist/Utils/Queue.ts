export class Queue<T> implements Iterable<T> {
  /**
   * The internal array holding the elements of the queue.
   *
   * @private
   * @type {T[]}
   * @memberof Queue
   */
  protected _queue: T[];

  /**
   * Creates an instance of Queue.
   * @param {number} _maxSize - The maximum number of elements in the queue.
   * @memberof Queue
   */
  public constructor(_maxSize: number) {
    this._queue = [];
  }

  [Symbol.iterator](): Iterator<T, any, undefined> {
    return this._queue.values();
  }

  /**
   * A check if the queue contains any elements.
   *
   * @returns {boolean} True if the queue is empty.
   * @memberof Queue
   */
  public isEmpty(): boolean {
    return this._queue.length === 0;
  }

  public enqueue(item: T): void {
    this._queue.push(item); // post-increment adds 1 to length after insertion.
  }

  public dequeue(): T {
    if (this.isEmpty()) {
      throw new Error("Queue is empty");
    }

    return this._queue.pop()!;
  }

  /**
   * Returns the first element of a queue but doesn't remove it from the queue.
   *
   * @returns {T}
   * @memberof Queue
   */
  public peek(): T {
    if (this.isEmpty()) {
      throw new Error("Queue is empty");
    }

    return this._queue[0];
  }

  public queueContents(): void {
    console.log("Queue contents");
    this._queue.forEach((item, index) => {
      console.log(`queue[${index}]: ${item}`);
    });
  }

  public clear() : void {
    this._queue = [];
  }
}
