import { Queue } from "./Queue.ts";

export class ShuffleableQueue<T> extends Queue<T> {
  private _shuffled: boolean;

  public constructor(maxSize: number) {
    super(maxSize);

    this._shuffled = false;
  }

  public isShuffled(): boolean {
    return this._shuffled;
  }

  // public enqueue(item: T): void {
  //   super.enqueue(item);
  //   this._shuffled = false;
  // }

  public shuffle(): this {
    let queue = this._queue;

    let currentIndex = queue.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 != currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = queue[currentIndex];
      queue[currentIndex] = queue[randomIndex];
      queue[randomIndex] = temporaryValue;
    }

    this._shuffled = true;

    return this;
  }

  public size(): number {
    return this._queue.length;
  }
}
