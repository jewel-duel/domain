import { CDLL } from "../Utils/CircularDoublyLinkedList.ts";
import type { Player } from "./Player/Player.ts";

export class TurnManager {
  #playersArray: Player[];
  #players: CDLL.CircularDoublyLinkedList<
    Player
  > = new CDLL.CircularDoublyLinkedList();
  #turn = 1;
  #startingPlayer: Player;

  constructor(players: Player[]) {
    this.#playersArray = []
    players.forEach((player) => {
      this.#players.insert(player);
      this.#playersArray.push(player);
    });

    this.#startingPlayer = this.#players.current!.data!;
  }

  get currentPlayer(): Player {
    return this.#players.current!.data!;
  }

  get turn(): number {
    return this.#turn;
  }

  get isForward(): boolean {
    return this.#players.isForward();
  }

  public next(): Player {
    const nextPlayer = this.#players.current!.next!.data!;
    if (nextPlayer === this.#startingPlayer) {
      this.incrementTurn();
    }
    this.#players.proceed();
    return this.#players.current!.data!;
  }

  public peek(): Player {
    return this.#players.current!.next!.data!;
  }

  public players(): Player[] {
    return this.#playersArray
  }

  private incrementTurn(): void {
    if (this.#turn === 1) {
      this.#players.proceed();
      this.#players.reverse();
    }
    this.#turn++;
  }
}
