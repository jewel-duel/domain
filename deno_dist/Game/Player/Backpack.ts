import type { Item } from "../Items/Item.ts";
import { BackpackSlot, SlotFactory } from "./Slot.ts";

export class Backpack {
  #slots: BackpackSlot[];
  #capacity: number;

  public static Create() {
    return new Backpack(5);
  }

  private constructor(capacity: number) {
    this.#capacity = capacity;
    this.#slots = new Array<BackpackSlot>(capacity);
    for (let i = 0; i < this.#slots.length; i++) {
      this.#slots[i] = SlotFactory.BackpackSlot(i + 1);
    }
  }

  public get(index: number): BackpackSlot {
    return this.#slots[index];
  }

  public put(item: Item, index: number): void {
    if (index + 1 > this.#capacity) return;
    this.#slots[index].put(item);
  }

  public at(index: number): Item | null {
    if (index + 1 > this.#capacity) return null;
    return this.#slots[index].contents;
  }

  public swap(firstIndex: number, secondIndex: number): void {
    if (firstIndex === secondIndex) return;
    if (
      firstIndex + 1 > this.#capacity || secondIndex + 1 > this.#capacity
    ) {
      return;
    }
    const temp = this.#slots[firstIndex].contents as Item | null;
    const temp2 = this.#slots[secondIndex].contents as Item | null;
    if (temp === null) {
      this.#slots[secondIndex].clear();
    } else {
      this.#slots[secondIndex].put(temp);
    }
    if (temp2 === null) {
      this.#slots[firstIndex].clear();
    } else {
      this.#slots[firstIndex].put(temp2);
    }
  }

  get slots() : BackpackSlot[] {
    return this.#slots;
  }
}
