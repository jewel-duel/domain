import { ShuffleableQueue } from "../../Utils/ShuffleableQueue.ts";

export class PlayerColor {
  #color: string;

  public static Green(): PlayerColor {
    return new PlayerColor("green");
  }

  public static Blue(): PlayerColor {
    return new PlayerColor("blue");
  }

  public static Red(): PlayerColor {
    return new PlayerColor("red");
  }

  public static Yellow(): PlayerColor {
    return new PlayerColor("yellow");
  }

  public static Cyan(): PlayerColor {
    return new PlayerColor("cyan");
  }

  public static Magenta(): PlayerColor {
    return new PlayerColor("magenta");
  }

  private constructor(color: string) {
    this.#color = color;
  }

  get color(): string {
    return this.#color;
  }
}

export class PlayerColorFactory {
  private static instance: PlayerColorFactory;
  private _colorChoices: ShuffleableQueue<PlayerColor>;

  private constructor() {
    this._colorChoices = new ShuffleableQueue<PlayerColor>(6);

    this.populateColors();

    this._colorChoices.shuffle();
  }

  public static getInstance(): PlayerColorFactory {
    if (!PlayerColorFactory.instance) {
      PlayerColorFactory.instance = new PlayerColorFactory();
    }

    return PlayerColorFactory.instance;
  }

  public getRandomColor(): PlayerColor {
    if (this._colorChoices.isEmpty()) {
      this.populateColors();
      this.shuffleColors();
    }
    return this._colorChoices.dequeue();
  }

  public shuffleColors(): void {
    this._colorChoices.shuffle();
  }

  public populateColors(): void {
    if (!this._colorChoices.isEmpty()) {
      this._colorChoices.clear();
    }
    this._colorChoices.enqueue(PlayerColor.Red());
    this._colorChoices.enqueue(PlayerColor.Yellow());
    this._colorChoices.enqueue(PlayerColor.Green());
    this._colorChoices.enqueue(PlayerColor.Blue());
    this._colorChoices.enqueue(PlayerColor.Cyan());
    this._colorChoices.enqueue(PlayerColor.Magenta());
  }
}
