import type { HexCoord } from "../../Utils/HexCoord.ts";
import type { Color } from "../Colors/Color.ts";
import { Character } from "./Character.ts";
import type { PlayerActions } from "./PlayerActions.ts";
import { PlayerId } from "./PlayerId.ts";

export class Player {
  #name: string;
  #id: PlayerId;
  #connected: boolean;
  #character: Character;

  //#region Factories

  public static FromUser(
    userName: string,
    userId: string,
    color: Color,
  ): Player {
    return new Player(userName, new PlayerId(userId), color, true);
  }

  public static Create(
    name: string,
    id: PlayerId,
    color: Color,
    connected: boolean,
  ): Player {
    return new Player(name, id, color, connected);
  }

  //#endregion

  private constructor(
    name: string,
    id: PlayerId,
    color: Color,
    connected: boolean,
  ) {
    this.#name = name;
    this.#id = id;
    this.#connected = connected;
    this.#character = new Character(color);
  }

  get id(): PlayerId {
    return this.#id;
  }

  get name(): string {
    return this.#name;
  }

  get color(): Color {
    return this.#character.color;
  }

  get position(): HexCoord {
    return this.#character.position;
  }

  get actions(): PlayerActions {
    return this.#character.actions;
  }

  get character(): Character {
    return this.#character;
  }

  get isConnected(): boolean {
    return this.#connected;
  }

  public moveTo(dest: HexCoord) {
    this.#character.position = dest;
  }

  public connect(): void {
    this.#connected = true;
  }

  public disconnect(): void {
    this.#connected = false;
  }
}
