import { Category, CategoryType } from "../Items/Category.ts";
import type { Item } from "../Items/Item.ts";
import { PutInventoryError } from "./PutInventoryError.ts";

export class SlotFactory {
  public static BackpackSlot(index: number): BackpackSlot {
    const key = `backpack${index}`;
    return new BackpackSlot(key, CategoryType.All());
  }

  public static WearableSlot(key: string): WearableSlot {
    return new WearableSlot(key, [Category.Wearables]);
  }

  public static WieldableSlot(key: string): WieldableSlot {
    return new WieldableSlot(key, [Category.Wieldables]);
  }
}

export class Slot {
  #type: Slots;
  #contents: Item | null = null;
  #key: string;
  #categories: Category[];

  public constructor(key: string, type: Slots, categories: Category[]) {
    this.#key = key;
    this.#type = type;
    this.#categories = categories;
  }

  public put(item: Item | null): Item | null {
    const result = this.#contents;
    if (item === null) {
      this.#contents = null;
    } else {
      if (!this.#categories.includes(item.category)) {
        throw new PutInventoryError(item.category.toString(), this.#key);
      }
      this.#contents = item;
    }
    return result;
  }

  public clear(): Item | null {
    const result = this.#contents;
    this.#contents = null;
    return result;
  }

  get contents(): Item | null {
    return this.#contents;
  }

  get type(): Slots {
    return this.#type;
  }

  get key(): string {
    return this.#key;
  }

  get categories(): Category[] {
    return this.#categories;
  }

  public isEmpty(): boolean {
    return this.#contents == null;
  }
}

export const Slots = {
  Wearable: "wearable",
  Wieldable: "wieldable",
  Any: "any",
} as const;

export type Slots = typeof Slots[keyof typeof Slots];

export class SlotType {
  public static FromString(typeString: string): Slots {
    const typestring = typeString.toLowerCase();
    switch (typestring) {
      case "wearable":
        return Slots.Wearable;
      case "wieldable":
        return Slots.Wieldable;
      case "any":
        return Slots.Any;
      default:
        throw new Error(`Slot type of '${typeString}' not found.`);
    }
  }

  public static Types(): string[] {
    const result = [];
    for (const key in Slots) {
      if (Object.hasOwnProperty.call(Slots, key)) {
        result.push(key.toLowerCase());
      }
    }

    return result.sort();
  }

  public static All(): Slots[] {
    const result: Slots[] = [];
    for (const key in Slots) {
      result.push(SlotType.FromString(key));
    }

    return result.sort();
  }
}

export const SlotKeys = {
  LeftHand: "left_hand",
  RightHand: "right_hand",
  Head: "head",
  LeftArm: "left_arm",
  Body: "body",
  RightArm: "right_arm",
  LeftLeg: "left_leg",
  RightLeg: "right_leg",
} as const;

export type SlotKeys = typeof SlotKeys[keyof typeof SlotKeys];

export class SlotKeyType {
  public static FromString(typeString: string): SlotKeys {
    const typestring = typeString.toLowerCase();
    switch (typestring) {
      case "left_hand":
        return SlotKeys.LeftHand;
      case "right_hand":
        return SlotKeys.RightHand;
      case "head":
        return SlotKeys.Head;
      case "left_arm":
        return SlotKeys.LeftArm;
      case "body":
        return SlotKeys.Body;
      case "right_arm":
        return SlotKeys.RightArm;
      case "left_leg":
        return SlotKeys.LeftLeg;
      case "right_leg":
        return SlotKeys.RightLeg;
      default:
        throw new Error(`Slot key type of '${typeString}' not found.`);
    }
  }

  public static Types(): string[] {
    const result = [];
    for (const key in SlotKeys) {
      if (Object.hasOwnProperty.call(SlotKeys, key)) {
        result.push(key.toLowerCase());
      }
    }

    return result.sort();
  }

  public static All(): SlotKeys[] {
    const result: SlotKeys[] = [];
    for (const key in SlotKeys) {
      result.push(SlotKeyType.FromString(key));
    }

    return result.sort();
  }
}

export class BackpackSlot extends Slot {
  public constructor(key: string, categories: Category[]) {
    super(key, Slots.Any, categories);
  }
}

export class WieldableSlot extends Slot {
  public constructor(key: string, categories: Category[]) {
    super(key, Slots.Wieldable, categories);
  }
}

export class WearableSlot extends Slot {
  public constructor(key: string, categories: Category[]) {
    super(key, Slots.Wearable, categories);
  }
}
