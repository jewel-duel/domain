import type { Board } from "./Board.ts";
import type { IBoardStrategy } from "./Strategies/IBoardStrategy.ts";

export class BoardFactory {
  public static Create(strategy: IBoardStrategy): Board {
    return strategy.execute();
  }
}
