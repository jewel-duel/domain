import type { IBoardStrategy } from "./IBoardStrategy.ts";
import type { Board } from "../Board.ts";
import type { StrategyType } from "./StrategyFactory.ts";

export abstract class BaseBoardStrategy implements IBoardStrategy {
  protected _type: StrategyType;
  abstract execute(): Board;

  constructor(type: StrategyType) {
    this._type = type;
  }

  get type(): StrategyType {
    return this._type;
  }
}
