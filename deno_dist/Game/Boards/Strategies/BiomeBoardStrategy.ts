import { TileStack } from "../TileStack.ts";
import { Axis, HexCoord, HexVector } from "../../../Utils/HexCoord.ts";
import { BiomeType } from "../Biomes/BiomeType.ts";
import { HexMap } from "../../../Utils/HexMap.ts";
import { Board } from "../Board.ts";
import { Queue } from "../../../Utils/Queue.ts";
import { ShuffleableQueue } from "../../../Utils/ShuffleableQueue.ts";
import { GameTile } from "../../GameTiles/GameTile.ts";
import { StrategyType } from "./StrategyFactory.ts";
import { BaseBoardStrategy } from "./BaseBoardStrategy.ts";

export interface IBiomeBoardStrategyOptions {
  size: number;
}

export class BiomeBoardStrategy extends BaseBoardStrategy {
  #options: IBiomeBoardStrategyOptions;
  #biomes: ShuffleableQueue<BiomeType>;

  public constructor(options: IBiomeBoardStrategyOptions = { size: 5 }) {
    super(StrategyType.Biome);
    if (options.size < 2) {
      throw new Error(
        `A board must have a larger radius than ${options.size} tiles.`,
      );
    }

    this.#options = options;
    this.#biomes = this.createBiomeTypeStack();
  }

  execute(): Board {
    return this.createMap(this.#biomes);
  }

  get options(): IBiomeBoardStrategyOptions {
    return this.#options;
  }

  private createBiomeTypeStack(): ShuffleableQueue<BiomeType> {
    let biomes = new ShuffleableQueue<BiomeType>(6);
    biomes.enqueue(BiomeType.Swamp);
    biomes.enqueue(BiomeType.Swamp);
    biomes.enqueue(BiomeType.Desert);
    biomes.enqueue(BiomeType.Desert);
    biomes.enqueue(BiomeType.Tundra);
    biomes.enqueue(BiomeType.Tundra);

    biomes.shuffle();

    return biomes;
  }

  private createMap(biomeStack: ShuffleableQueue<BiomeType>): Board {
    const board = Board.New(this.type);

    let axes = new Queue<HexVector>(6);
    axes.enqueue(HexVector.Up(Axis.Q));
    axes.enqueue(HexVector.Down(Axis.Q));
    axes.enqueue(HexVector.Up(Axis.R));
    axes.enqueue(HexVector.Down(Axis.R));
    axes.enqueue(HexVector.Up(Axis.S));
    axes.enqueue(HexVector.Down(Axis.S));

    while (!biomeStack.isEmpty()) {
      let biome = biomeStack.dequeue();
      let wedge: Wedge;
      let axis = axes.dequeue();
      if (biome === BiomeType.Swamp) {
        wedge = Wedge.Swamp(axis);
      } else if (biome === BiomeType.Desert) {
        wedge = Wedge.Desert(axis);
      } else {
        wedge = Wedge.Tundra(axis);
      }

      const it = wedge.hexMap.keys();
      let result = it.next();

      while (!result.done) {
        board.addTile(
          HexCoord.FromString(result.value),
          wedge.getGameTile(result.value),
        );
        result = it.next();
      }
    }

    board.addTile(HexCoord.Origin(), GameTile.Explored("volcano"));

    return board;
  }
}

export class Wedge {
  private _terrainTiles: TileStack;
  private _hexMap: HexMap<GameTile>;

  //#region Factories

  public static Desert(hexVector: HexVector): Wedge {
    return new Wedge(5, "desert", hexVector);
  }

  public static Tundra(hexVector: HexVector): Wedge {
    return new Wedge(5, "tundra", hexVector);
  }

  public static Swamp(hexVector: HexVector): Wedge {
    return new Wedge(5, "swamp", hexVector);
  }

  //#endregion

  private constructor(radius: number, biomeType: BiomeType, vector: HexVector) {
    let size = this.calculateTriangleNumber(radius);

    if (biomeType === "desert") {
      this._terrainTiles = TileStack.CreateDesertStack(size);
    } else if (biomeType === "swamp") {
      this._terrainTiles = TileStack.CreateSwampStack(size);
    } else if (biomeType === "tundra") {
      this._terrainTiles = TileStack.CreateTundraStack(size);
    } else {
      throw new Error("Biome type not recognized.");
    }

    this._hexMap = this.packHexMap(radius, vector, this._terrainTiles);
  }

  get tiles(): TileStack {
    return this._terrainTiles;
  }

  get hexMap(): HexMap<GameTile> {
    return this._hexMap;
  }

  public getGameTile(coords: string): GameTile {
    return this._hexMap.getWithString(coords)!;
  }

  public calculateTriangleNumber(radius: number): number {
    let n = radius;
    return (n * n + n) / 2;
  }

  private packHexMap(
    radius: number,
    vector: HexVector,
    stack: TileStack,
  ): HexMap<GameTile> {
    let hexMap = HexMap.New<GameTile>();

    // find the starting coordinate
    let q: number = 0;
    let r: number = 0;
    let s: number = 0;
    if (vector.axis === Axis.Q) {
      q = vector.direction;
    } else if (vector.axis === Axis.R) {
      r = vector.direction;
    } else {
      r = vector.direction * -1;
      q = vector.direction;
    }
    s = -q - r;

    let coordUnit = HexCoord.Create(q, r, s);
    let coord = coordUnit.multiply(radius);
    let coordStart = coordUnit.multiply(radius);
    let rowVector = coordUnit.shiftRight();

    while (!stack.isEmpty()) {
      // place the starting tile (this should be a gemTile)
      let tile = stack.pull();
      if (tile === "gemTemple") {
        hexMap.add(coord.toString(), GameTile.Explored(tile));
      } else {
        hexMap.add(coord.toString(), GameTile.Unexplored(tile));
      }

      // loop along the row
      for (let i = 0; i < radius - 1; i++) {
        coord = coord.add(rowVector);
        hexMap.add(coord.toString(), GameTile.Unexplored(stack.pull()));
      }

      // increment the coord to the next row
      coordStart = coordStart.subtract(coordUnit);
      coord = HexCoord.Create(coordStart.q, coordStart.r, coordStart.s);
      radius--;
    }

    return hexMap;
  }
}
