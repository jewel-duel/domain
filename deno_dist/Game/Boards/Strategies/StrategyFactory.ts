import type { IBoardStrategy } from "./IBoardStrategy.ts";
import {
  BiomeBoardStrategy,
  IBiomeBoardStrategyOptions,
} from "./BiomeBoardStrategy.ts";
import {
  IRandomBoardStrategyOptions,
  RandomBoardStrategy,
} from "./RandomBoardStrategy.ts";

export class StrategyFactory {
  public static Create(
    strategy: string = "random",
    options?: {},
  ): IBoardStrategy {
    let result: IBoardStrategy;
    if (strategy === "biome") {
      result = new BiomeBoardStrategy(options as IBiomeBoardStrategyOptions);
    } else {
      result = new RandomBoardStrategy(options as IRandomBoardStrategyOptions);
    }

    return result;
  }
}

export enum StrategyType {
  Biome = "biome",
  Random = "random",
}
