import type { Board } from "../Board.ts";
import type { StrategyType } from "./StrategyFactory.ts";

export interface IBoardStrategy {
  type: StrategyType;
  execute(): Board;
}
