export const Terrain = {
  Grassland: "grassland",
  GemTemple: "gemTemple",
  Hills: "hills",
  Mountain: "mountain",
  Volcano: "volcano",
  Marsh: "marsh",
  Sand: "sand",
  Portal: "portal",
  Village: "village",
  Forest: "forest",
  Ice: "ice",
} as const;

export type Terrain = typeof Terrain[keyof typeof Terrain];

export class TerrainType {
  public static FromString(typeString: string): Terrain {
    switch (typeString.toLowerCase()) {
      case "ice":
        return Terrain.Ice;
      case "hills":
        return Terrain.Hills;
      case "gemtemple":
        return Terrain.GemTemple;
      case "grassland":
        return Terrain.Grassland;
      case "forest":
        return Terrain.Forest;
      case "marsh":
        return Terrain.Marsh;
      case "mountain":
        return Terrain.Mountain;
      case "portal":
        return Terrain.Portal;
      case "sand":
        return Terrain.Sand;
      case "village":
        return Terrain.Village;
      case "volcano":
        return Terrain.Volcano;
      default:
        throw new Error(`Terrain type of '${typeString}' not found.`);
    }
  }

  public static Types(): string[] {
    const result = [];
    for (const key in Terrain) {
      if (Object.hasOwnProperty.call(Terrain, key)) {
        result.push(key.toLowerCase());
      }
    }

    return result.sort();
  }

  public static PlaceableTypes(): string[] {
    const result = [];
    for (const key in Terrain) {
      if (
        Object.hasOwnProperty.call(Terrain, key) && key !== "Volcano" && key !== "GemTemple"
      ) {
        result.push(key.toLowerCase());
      }
    }

    return result.sort();
  }
}
