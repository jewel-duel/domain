import type { BiomeType } from "./Biomes/BiomeType.ts";
import { ShuffleableQueue } from "../../Utils/ShuffleableQueue.ts";
import { Terrain } from "./Terrains/TerrainType.ts";
import { Biome } from "./Biomes/Biome.ts";

export class TileStack {
  private _tiles: ShuffleableQueue<Terrain>;
  private _biome: Biome;

  public static CreateSwampStack(size: number): TileStack {
    let stack = new TileStack("swamp", size);

    stack.shuffle();
    stack.pushTerrain(Terrain.GemTemple);

    return stack;
  }

  public static CreateDesertStack(size: number): TileStack {
    let stack = new TileStack("desert", size);

    stack.shuffle();
    stack.pushTerrain(Terrain.GemTemple);

    return stack;
  }

  public static CreateTundraStack(size: number): TileStack {
    let stack = new TileStack("tundra", size);

    stack.shuffle();
    stack.pushTerrain(Terrain.GemTemple);

    return stack;
  }

  private constructor(biome: BiomeType, size: number) {
    if (biome === "desert") {
      this._biome = Biome.Desert();
    } else if (biome === "swamp") {
      this._biome = Biome.Swamp();
    } else if (biome == "tundra") {
      this._biome = Biome.Tundra();
    } else {
      throw new Error("Unrecognized biome.");
    }

    this._tiles = this.drawCards(size);
  }

  public isEmpty(): boolean {
    return this._tiles.isEmpty();
  }

  public pull(): Terrain {
    return this._tiles.dequeue();
  }

  public size(): number {
    return this._tiles.size();
  }

  private drawCards(stackSize: number): ShuffleableQueue<Terrain> {
    // TODO: make this work for sizes other than 14
    let result = new ShuffleableQueue<Terrain>(stackSize);
    result.enqueue(Terrain.Village);
    result.enqueue(Terrain.Portal);
    result.enqueue(Terrain.Mountain);
    result.enqueue(this._biome.specialTile);
    result.enqueue(this._biome.specialTile);
    result.enqueue(Terrain.Forest);
    result.enqueue(Terrain.Forest);
    result.enqueue(Terrain.Forest);
    result.enqueue(Terrain.Hills);
    result.enqueue(Terrain.Hills);
    result.enqueue(Terrain.Hills);
    result.enqueue(Terrain.Grassland);
    result.enqueue(Terrain.Grassland);
    result.enqueue(Terrain.Grassland);

    return result;
  }

  private shuffle(): void {
    this._tiles.shuffle();
  }

  private pushTerrain(terrain: Terrain): void {
    this._tiles.enqueue(terrain);
  }
}
