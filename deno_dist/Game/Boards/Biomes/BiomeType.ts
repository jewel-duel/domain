export const BiomeType = {
  Swamp: "swamp",
  Desert: "desert",
  Tundra: "tundra",
} as const;

export type BiomeType = typeof BiomeType[keyof typeof BiomeType];
