export const Kind = {
    Shield: "shield",
    Sword: "sword",
    Bow: "bow",
    Armor: "armor",
    Gold: "gold",
    Wood: "wood",
    Iron: "iron",
    Bronze: "bronze",
    Gem: "gem",
    Duck: "duck"
  } as const;
  
  export type Kind = typeof Kind[keyof typeof Kind];
  
  export class KindType {
    public static FromString(typeString: string): Kind {
        const typestring = typeString.toLowerCase();
      switch (typestring) {
        case "shield":
            return Kind.Shield
        case "sword":
            return Kind.Sword
        case "bow":
            return Kind.Bow
        case "armor":
            return Kind.Armor
        case "gold":
            return Kind.Gold
        case "wood":
            return Kind.Wood
        case "iron":
            return Kind.Iron
        case "bronze":
            return Kind.Bronze
        case "gem":
            return Kind.Gem
        case "duck":
            return Kind.Duck
        default:
          throw new Error(`Resource type of '${typeString}' not found.`);
      }
    }
  
    public static Types(): string[] {
      const result = [];
      for (const key in Kind) {
        if (Object.hasOwnProperty.call(Kind, key)) {
          result.push(key.toLowerCase());
        }
      }
  
      return result.sort();
    }
  
    public static All(): Kind[] {
      const result : Kind[] = [];
      for (const key in Kind) {
        result.push(KindType.FromString(key))
      }
  
      return result.sort();
    }
  }
  