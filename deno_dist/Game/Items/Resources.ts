export const Resources = {
    Bronze: "bronze",
    Iron: "iron",
    Wood: "wood"
  } as const;
  
  export type Resources = typeof Resources[keyof typeof Resources];
  
  export class ResourceType {
    public static FromString(typeString: string): Resources {
      switch (typeString.toLowerCase()) {
        case "bronze":
          return Resources.Bronze
        case "iron":
          return Resources.Iron
        case "wood":
          return Resources.Wood
        default:
          throw new Error(`Resource type of '${typeString}' not found.`);
      }
    }
  
    public static Types(): string[] {
      const result = [];
      for (const key in Resources) {
        if (Object.hasOwnProperty.call(Resources, key)) {
          result.push(key.toLowerCase());
        }
      }
  
      return result.sort();
    }
  
    public static All(): Resources[] {
      const result : Resources[] = [];
      for (const key in Resources) {
        result.push(ResourceType.FromString(key))
      }
  
      return result.sort();
    }
  }
  