export const Category = {
  Wearables: "wearables",
  Wieldables: "wieldables",
  Resource: "resource",
  Specials: "specials"
} as const;

export type Category = typeof Category[keyof typeof Category];

export class CategoryType {
  public static FromString(typeString: string): Category {
    switch (typeString.toLowerCase()) {
      case "wearables":
        return Category.Wearables;
      case "wieldables":
        return Category.Wieldables;
      case "resource":
        return Category.Resource
      case "specials":
        return Category.Specials
      default:
        throw new Error(`Category type of '${typeString}' not found.`);
    }
  }

  public static Types(): string[] {
    const result = [];
    for (const key in Category) {
      if (Object.hasOwnProperty.call(Category, key)) {
        result.push(key.toLowerCase());
      }
    }

    return result.sort();
  }

  public static All(): Category[] {
    const result : Category[] = [];
    for (const key in Category) {
      result.push(CategoryType.FromString(key))
    }

    return result.sort();
  }
}
