import { Category } from "./Category.ts";
import { generate } from "../../Utils/v4.ts";
import type { Resources } from "./Resources.ts";
import { Craftable } from "./Craftable.ts";
import { Kind } from "./Kind.ts";

export class Wearable extends Craftable {

  public static Armor(type: Resources): Wearable {
    const id = generate();
    return new Wearable(id, Kind.Armor, type, [type], 1);
  }

  protected constructor(id: string, kind: Kind, type: Resources, materials: Resources[], price: number) {
    super(id, kind, Category.Wearables, type, materials, price);
  }
}
