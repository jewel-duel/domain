import generateRandomString from "../Utils/RandomString.ts";
import type { Player } from "./Player/Player.ts";
import type { Board } from "./Boards/Board.ts";
import type { IBoardStrategy } from "./Boards/Strategies/IBoardStrategy.ts";
import { BoardFactory } from "./Boards/BoardFactory.ts";
import { StrategyFactory } from "./Boards/Strategies/StrategyFactory.ts";
import { TurnManager } from "./TurnManager.ts";
import type { HexCoord } from "../Utils/HexCoord.ts";
import { HexMap } from "../Utils/HexMap.ts";

export interface IGameOptions {
  key: string;
  players: Player[];
  strategy: IBoardStrategy;
}

/**
 * The Options for starting  a game
 *
 * @export
 * @class GameOptions
 * @implements {IGameOptions}
 */
export class GameOptions implements IGameOptions {
  #key: string;
  #players: Player[];
  #strategy: IBoardStrategy;

  constructor(
    key: string,
    players: Player[],
    strategy: IBoardStrategy = StrategyFactory.Create("random"),
  ) {
    this.#key = key;
    this.#strategy = strategy;
    this.#players = players;
  }

  /**
   * The unique identifier for the game
   *
   * @readonly
   * @type {string}
   * @memberof GameOptions
   */
  get key(): string {
    return this.#key;
  }

  /**
   * The player who will play the game
   *
   * @readonly
   * @type {Player[]}
   * @memberof GameOptions
   */
  get players(): Player[] {
    return this.#players;
  }

  /**
   * The board generation strategy
   *
   * @readonly
   * @type {IBoardStrategy}
   * @memberof GameOptions
   */
  get strategy(): IBoardStrategy {
    return this.#strategy;
  }
}

/**
 * Game
 *
 * @export
 * @class Game
 */
export class Game {
  private _started: boolean = false;
  private _key: string;
  private _players: Player[];
  private _board: Board | null = null;
  private _creationDate: number;
  private _turnManager: TurnManager;

  //#region Factories

  public static Create(options: IGameOptions): Game {
    return new Game(options);
  }

  //#endregion

  /**
   * Creates an instance of Game.
   * @memberof Game
   * @constructor
   */
  private constructor(options: IGameOptions) {
    this._key = options.key || generateRandomString();
    this._players = [];

    // TODO: validate players length
    options.players.forEach((player) => {
      this._addPlayer(player);
    });

    if (this._players.length > 0) {
      this._board = BoardFactory.Create(options.strategy);
    }

    this._turnManager = new TurnManager(this._players);

    this._creationDate = Date.now();
    this._started = true;
  }

  public start(generationStrategy?: IBoardStrategy): void {
    generationStrategy = generationStrategy || StrategyFactory.Create("random");
    if (this._players.length > 0) {
      this._board = BoardFactory.Create(generationStrategy!);
      this._started = true;
    }
  }

  private _addPlayer(player: Player): void {
    if (!this._started) this._players.push(player);
    // TODO: throw error if game has already started.
  }

  //#region Getters

  get key(): string {
    return this._key;
  }

  get started(): boolean {
    return this._started;
  }

  get players(): Player[] {
    return this._players;
  }

  public getPlayerById(playerId: string): Player | null {
    const result = this._players.find((x) => x.id.toString() === playerId);
    return !!result ? result : null;
  }

  public disconnectPlayer(player: Player): void {
    const playerIdString = player.id.toString();
    const p = this._players.find((x) => x.id.toString() === playerIdString);
    if (p) {
      p.disconnect();
    }
  }

  public playersConnected(): boolean {
    const result = this._players.find((x) => x.isConnected);
    return result ? true : false;
  }

  get board(): Board | null {
    return this._board;
  }

  get creationDate(): number {
    return this._creationDate;
  }

  get turnManager(): TurnManager {
    return this._turnManager;
  }

  // Methods

  public nextTurn(): void {
    this._turnManager.next();
    this._turnManager.currentPlayer.actions.resetCount();
  }

  public movePlayer(player: Player, dest: HexCoord): void {
    if (this._board == null) return;
    if (player.id != this._turnManager.currentPlayer.id) {
      throw new Error("Player cannot move out of turn.");
    }
    if (!this._board.coordExists(dest)) {
      throw new Error("Tile does not exist.");
    }
    if (!HexMap.AreNeighbors(player.position, dest)) {
      throw new Error("Can only move to adjacent tile.");
    }
    if (player.actions.isEmpty) {
      throw new Error("Player out of actions.");
    }

    // Calculate actions needed to move.
    let actions_required = 1;
    const dest_tile = this._board.getTile(dest);
    const start_tile = this._board.getTile(player.position);
    const elevation_diff = dest_tile.elevation - start_tile.elevation;
    if (elevation_diff > 0) {
      actions_required = actions_required + elevation_diff;
    }

    if (player.actions.actions < actions_required) {
      throw new Error("Not enough actions to move to tile");
    }

    player.moveTo(dest);
    player.actions.spendAction(actions_required);
  }

  //#endregion
}
