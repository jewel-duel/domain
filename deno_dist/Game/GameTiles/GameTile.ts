import { Terrain, TerrainType } from "../Boards/Terrains/TerrainType.ts";

export class GameTile {
  private _type: Terrain;
  private _elevation: number;
  private _isExplored: boolean;

  public static Unexplored(type: Terrain): GameTile {
    return new GameTile(type, false);
  }

  public static Explored(type: Terrain): GameTile {
    return new GameTile(type, true);
  }

  public static Hydrate(type: string, isExplored: boolean): GameTile {
    return new GameTile(TerrainType.FromString(type), isExplored);
  }

  private constructor(type: Terrain, explored: boolean = false) {
    this._type = type;
    this._isExplored = explored;
    if (type === "volcano") {
      this._elevation = 4;
    } else if (type === "mountain") {
      this._elevation = 3;
    } else if (type === "hills") {
      this._elevation = 2;
    } else {
      this._elevation = 1;
    }
  }

  public explore(): void {
    this._isExplored = true;
  }

  public unexplore(): void {
    this._isExplored = false;
  }

  get isExplored(): boolean {
    return this._isExplored;
  }

  get type(): Terrain {
    return this._type;
  }

  get elevation(): number {
    return this._elevation;
  }
}
