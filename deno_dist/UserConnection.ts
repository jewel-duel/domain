import type { User } from "./Users/User.ts";
import type { Connection } from "./Connection.ts";

export class UserConnection {
  #user: User;
  #connection: Connection;

  constructor(user: User, connection: Connection) {
    this.#connection = connection;
    this.#user = user;
  }

  get connection(): Connection {
    return this.#connection;
  }

  get user(): User {
    return this.#user;
  }
}
